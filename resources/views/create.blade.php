<!doctype html>
<html lang="en">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Create</title>
    </head>

    <body>

        <form class="form" method="POST" action="{{ route('student.store') }}" enctype="multipart/form-data">
            @csrf
            
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingInput" placeholder="example" name="name">
                <label for="floatingInput">Name</label>
            </div>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingInput" placeholder="example" name="email">
                <label for="floatingInput">Email</label>
            </div>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingInput" placeholder="password" name="password">
                <label for="floatingInput">Password</label>
            </div>
            <div class="mb-3">
                <input class="form-control" type="file" name="file">
            </div>
            <button class="btn btn-primary" type="submit" style="width:100%;">Create</button>
        </form>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>

<style>

    .form {
    width: 500px;
    margin: 150px auto;
    padding: 30px;
    }

</style>